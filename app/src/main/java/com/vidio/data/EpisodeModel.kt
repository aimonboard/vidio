package com.vidio.data

import com.vidio.utils.DownloadStatus

data class EpisodeModel (
    val id: Int,
    val title: String,
    val duration: String,
    val thumbnailUrl: String,
    val description: String,
    val isFree: Boolean,
    val downloadUrl: String,
    var downloadStatus: DownloadStatus
)