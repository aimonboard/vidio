package com.vidio.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vidio.data.EpisodeModel
import com.vidio.databinding.ItemEpisodeBinding
import com.vidio.utils.DownloadStatus

class EpisodeAdapter (private val episodes: List<EpisodeModel>): RecyclerView.Adapter<EpisodeAdapter.EpisodeViewHolder>() {

    private var listener: EpisodeAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(ItemEpisodeBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(episodes[position])
    }

    override fun getItemCount(): Int = episodes.size

    fun setListener(listener: EpisodeAdapterListener) {
        try {
            this.listener = listener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateDownloadStatus(pos: Int, status: DownloadStatus) {
        episodes[pos].downloadStatus = status
        notifyItemChanged(pos)
    }

    inner class EpisodeViewHolder(private val binding: ItemEpisodeBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(episode: EpisodeModel){
            binding.title.text = episode.title

            when (episode.downloadStatus) {
                DownloadStatus.DOWNLOAD_FINISH -> {
                    binding.btnDownload.visibility = View.GONE
                    binding.loading.visibility = View.GONE
                }
                DownloadStatus.NOT_DOWNLOADED -> {
                    binding.btnDownload.visibility = View.VISIBLE
                    binding.loading.visibility = View.GONE
                }
                DownloadStatus.DOWNLOADING -> {
                    binding.btnDownload.visibility = View.GONE
                    binding.loading.visibility = View.VISIBLE
                }
            }

            binding.btnDownload.setOnClickListener {
                binding.loading.visibility = View.VISIBLE
                listener?.onEpisodeClick(adapterPosition, episode.downloadUrl)
            }

            Glide.with(binding.root)
                .load(episode.thumbnailUrl)
                .into(binding.image)
        }
    }

    interface EpisodeAdapterListener {
        fun onEpisodeClick(pos: Int, url: String)
    }

}