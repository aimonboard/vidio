package com.vidio.utils

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import java.io.File

class Download (private val activity: Activity, private val downloadListener: DownloadListener) {

    fun startDownload(pos: Int, url: String) {
        val format = url.substring(url.lastIndexOf(".") + 1)
        val fileName = "Episode ${pos+1}"

        val directory = File(Environment.DIRECTORY_DOWNLOADS)
        if (!directory.exists()) directory.mkdirs()

        val downloadManager = activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri).apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            setAllowedOverRoaming(false)
            setTitle("Downloading $fileName")
            setDescription("")
            setDestinationInExternalPublicDir(
                directory.toString(),
                "Vidio/$fileName.$format"
            )
        }

        val downloadId = downloadManager.enqueue(request)
        val query = DownloadManager.Query().setFilterById(downloadId)
        Thread {
            var downloading = true
            while (downloading) {
                val cursor = downloadManager.query(query)
                cursor.moveToFirst()

                activity.runOnUiThread {
                    when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
                        DownloadManager.STATUS_FAILED -> {
                            downloading = false
                            downloadListener.onDownloadCallback(pos, DownloadStatus.NOT_DOWNLOADED)
                        }
                        DownloadManager.STATUS_PAUSED -> {
                            downloadListener.onDownloadCallback(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_PENDING -> {
                            downloadListener.onDownloadCallback(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_RUNNING -> {
                            downloadListener.onDownloadCallback(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_SUCCESSFUL -> {
                            downloading = false
                            downloadListener.onDownloadCallback(pos, DownloadStatus.DOWNLOAD_FINISH)
                        }
                    }
                    cursor.close()
                }
            }
        }.start()
    }
}

interface DownloadListener {
    fun onDownloadCallback(pos: Int, downloadStatus: DownloadStatus)
}