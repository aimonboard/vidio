package com.vidio

import androidx.lifecycle.ViewModel
import com.vidio.data.EpisodeModel
import com.vidio.utils.DownloadStatus

class MainActivityViewModel : ViewModel() {

    private val videoUrl1 = "https://download.samplelib.com/mp4/sample-30s.mp4"
    private val videoUrl2 = "https://filesamples.com/samples/video/mp4/sample_1280x720.mp4"

    fun generateDummyData(): List<EpisodeModel> {
        val episodeData = mutableListOf<EpisodeModel>()

        for (i in 0 until 2) {
            episodeData.add(
                EpisodeModel(
                    id = i,
                    title = "Episode ${i + 1}",
                    duration = "00:00",
                    thumbnailUrl = "https://cdn.freelogovectors.net/wp-content/uploads/2021/12/vidio-logo-freelogovectors.net_.png",
                    description = "Description ${i+1}",
                    isFree = true,
                    downloadUrl = if (i==0) videoUrl1 else videoUrl2,
                    downloadStatus = DownloadStatus.NOT_DOWNLOADED
                )
            )
        }
        return episodeData
    }

}