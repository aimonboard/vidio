package com.vidio

import android.Manifest
import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.vidio.adapter.EpisodeAdapter
import com.vidio.databinding.ActivityMainBinding
import java.io.File
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.vidio.utils.Download
import com.vidio.utils.DownloadListener
import com.vidio.utils.DownloadStatus

class MainActivity : AppCompatActivity() {

    private val permissionRequestCode = 3543

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: EpisodeAdapter
    private lateinit var download: Download
    private val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        askPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty()) {
                setupDownload()
            }
        }
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
    }

    private fun askPermission() {
        if (!checkPermission()) {
            requestPermission()
        } else {
            setupDownload()
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), permissionRequestCode
        )
    }

    private fun setupDownload() {
        adapter = EpisodeAdapter(viewModel.generateDummyData())
        binding.recyclerEpisode.layoutManager = LinearLayoutManager(this)
        binding.recyclerEpisode.adapter = adapter

        download = Download(this, object : DownloadListener {
            override fun onDownloadCallback(pos: Int, downloadStatus: DownloadStatus) {
                adapter.updateDownloadStatus(pos, downloadStatus)
            }
        })

        adapter.setListener(object : EpisodeAdapter.EpisodeAdapterListener {
            override fun onEpisodeClick(pos: Int, url: String) {
//                downloadVideo(
//                    pos = pos,
//                    url = url
//                )

                download.startDownload(
                    pos = pos,
                    url = url
                )
            }
        })
    }

    @SuppressLint("Range")
    private fun downloadVideo(pos: Int, url: String) {
        val directory = File(Environment.DIRECTORY_DOWNLOADS)
        if (!directory.exists()) directory.mkdirs()

        val downloadManager = this.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri).apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            setAllowedOverRoaming(false)
            setTitle("Downloading Episode ${pos+1}")
            setDescription("")
            setDestinationInExternalPublicDir(
                directory.toString(),
                "Episode ${pos+1}"
            )
        }

        val downloadId = downloadManager.enqueue(request)
        val query = DownloadManager.Query().setFilterById(downloadId)
        Thread {
            var downloading = true
            while (downloading) {
                val cursor = downloadManager.query(query)
                cursor.moveToFirst()

                this.runOnUiThread {
                    when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
                        DownloadManager.STATUS_FAILED -> {
                            downloading = false
                            adapter.updateDownloadStatus(pos, DownloadStatus.NOT_DOWNLOADED)
                        }
                        DownloadManager.STATUS_PAUSED -> {
                            adapter.updateDownloadStatus(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_PENDING -> {
                            adapter.updateDownloadStatus(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_RUNNING -> {
                            adapter.updateDownloadStatus(pos, DownloadStatus.DOWNLOADING)
                        }
                        DownloadManager.STATUS_SUCCESSFUL -> {
                            downloading = false
                            adapter.updateDownloadStatus(pos, DownloadStatus.DOWNLOAD_FINISH)
                        }
                    }
                    cursor.close()
                }
            }
        }.start()
    }
}