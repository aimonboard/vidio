## Adi Ilham - Android Developer


## How to Run program

- Download the .zip file from Google Drive (link below).
- Once the download is complete, Extract the file.
- Open Android Studio and open the project (File-Open).
- Wait until all the gradle processes are finished.
- Connect Android Studio with emulator or physical device.
- Look for the "Run" icon on top of Android Studio.
- Press "Run" and wait until the process is complete.

## How to Test program

- Download the .apk file from Google Drive (link below).
- Once the download is complete, Open the file from your phone.
- Allow permission for install file from outside play store.
- Wait until installation are finished.
- Enjoy the program

## Download Source

- https://drive.google.com/drive/folders/1CYCTql6lU1QSuSN_aP8TAHumF2TVV4Ai?usp=sharing

